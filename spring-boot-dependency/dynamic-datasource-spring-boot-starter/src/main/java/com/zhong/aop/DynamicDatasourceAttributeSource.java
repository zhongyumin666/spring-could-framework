package com.zhong.aop;

import java.lang.reflect.Method;

/**
 * @author zhongyumin
 * @date 2021/7/15-下午4:44
 */
public interface DynamicDatasourceAttributeSource {


    default boolean isCandidateClass(Class<?> targetClass) {
        return true;
    }


    DynamicDatasourceAttribute getDynamicDatasourceAttribute(Method method, Class targetClass);
}
