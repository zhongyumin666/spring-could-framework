package com.zhong.aop;

/**
 * 资源线程传递类
 *
 * @author zhongyumin
 * @date 2021/7/20-上午9:51
 */
public class DynamicDatasourceAttributeSourceHolder {
    private static final DynamicDatasourceAttributeSourceHolder holder = new DynamicDatasourceAttributeSourceHolder();

    private DynamicDatasourceAttributeSourceHolder() {
    }

    public static DynamicDatasourceAttributeSourceHolder getInstance() {
        return holder;
    }

    private final ThreadLocal<DynamicDatasourceAttribute> binder = new ThreadLocal<DynamicDatasourceAttribute>();

    public DynamicDatasourceAttribute getCurrDynamicDatasourceAttribute() {
        return binder.get();
    }

    public void setDynamicDatasourceAttribute(DynamicDatasourceAttribute dynamicDatasourceAttribute) {
        binder.set(dynamicDatasourceAttribute);
    }

    public void removeDynamicDatasourceAttribute() {
        binder.remove();
    }
}
