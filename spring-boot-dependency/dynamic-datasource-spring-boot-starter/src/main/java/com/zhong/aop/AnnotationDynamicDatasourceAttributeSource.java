package com.zhong.aop;

import org.springframework.aop.support.AopUtils;
import org.springframework.core.MethodClassKey;
import org.springframework.util.ClassUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zhongyumin
 * @date 2021/7/15-下午4:33
 */
public class AnnotationDynamicDatasourceAttributeSource implements DynamicDatasourceAttributeSource {
    DynamicDatasourceAnnotationParser parser = new DynamicDatasourceAnnotationParser();

    private final Map<Object, DynamicDatasourceAttribute> attributeCache = new ConcurrentHashMap<>(1024);

    @Override
    public boolean isCandidateClass(Class<?> targetClass) {
        return parser.isCandidateClass(targetClass);
    }

    /**
     * 还需要优化 key太多了
     * @param method
     * @param targetClass
     * @return
     */
    @Override
    public DynamicDatasourceAttribute getDynamicDatasourceAttribute(Method method, Class targetClass) {
        MethodClassKey cacheKey = new MethodClassKey(method, targetClass);
        //1防止重复解析类，先从缓存获取
        if(attributeCache.containsKey(cacheKey)){
            return attributeCache.get(cacheKey);
        }

        DynamicDatasourceAttribute dynamicDatasourceAttribute=null;
        //2第一先看目标类实际方法类型

        //2.1和事务保持一致,只有公共方法可以被代理
        if(!Modifier.isPublic(method.getModifiers())){
             dynamicDatasourceAttribute = new DynamicDatasourceAttribute(null, 0, null, false);
            //不可用对象
            attributeCache.put(cacheKey,dynamicDatasourceAttribute);
            return  dynamicDatasourceAttribute;
        }
        //2.2 如果是接口方法需要转换一下实际方法类型
            // The method may be on an interface, but we need attributes from the target class.
            // If the target class is null, the method will be unchanged.
        Method specificMethod = AopUtils.getMostSpecificMethod(method, targetClass);
        //2.3方法,方法不行看类
         dynamicDatasourceAttribute = parser.parseDynamicDatasourceAnnotation(specificMethod);
        if(Objects.nonNull(dynamicDatasourceAttribute)&&ClassUtils.isUserLevelMethod(specificMethod)){
            attributeCache.put(cacheKey,dynamicDatasourceAttribute);
            return dynamicDatasourceAttribute;
        }
        //2.4看方法声明的实际类型
        dynamicDatasourceAttribute = parser.parseDynamicDatasourceAnnotation(specificMethod.getDeclaringClass());
        if(Objects.nonNull(dynamicDatasourceAttribute)){
            attributeCache.put(cacheKey,dynamicDatasourceAttribute);
            return dynamicDatasourceAttribute;
        }
        //2.5回查实际方法
       if(method!=specificMethod){
           dynamicDatasourceAttribute = parser.parseDynamicDatasourceAnnotation(method);
           if (Objects.nonNull(dynamicDatasourceAttribute)&&ClassUtils.isUserLevelMethod(method)) {
               attributeCache.put(cacheKey,dynamicDatasourceAttribute);
               return dynamicDatasourceAttribute;
           }
           dynamicDatasourceAttribute = parser.parseDynamicDatasourceAnnotation(targetClass);
           if (Objects.nonNull(dynamicDatasourceAttribute)) {
               attributeCache.put(cacheKey,dynamicDatasourceAttribute);
               return dynamicDatasourceAttribute;
           }
       }
       //最后缓存一下
        dynamicDatasourceAttribute = new DynamicDatasourceAttribute(null, 0, null, false);
        //不可用对象
        attributeCache.put(cacheKey,dynamicDatasourceAttribute);
        return dynamicDatasourceAttribute;
    }


}
