package com.zhong.aop;

/**
 * 注解最后都会转换成此作为传递存储
 * @author zhongyumin
 * @date 2021/7/15-下午4:46
 */
public class DynamicDatasourceAttribute {

    public DynamicDatasourceAttribute(RoleType role, int id, String name, boolean available) {
        this.role = role==null?RoleType.MASTER:role;
        this.id = id;
        this.name = name==null?"":name;
        this.available = available;
    }

    /**
     * 角色
     */
    private RoleType role;

    /**
     * id
     */
    private int id;

    /**
     * 名称
     */
    private String name;

    /**
     * 当前对象是否可用,主要用于类上标记没有标记但是方法上有标记存在多个方法并且有些方法没有注解,
     */
    private boolean available;

    /**
     * 获取数据源key的方法
     * @return
     */
    public   String getLookupKey(){
        return role+name+id;
    }

    /**
     * 是否可用
     * @return
     */
    public boolean isAvailable() {
        return available;
    }
}
