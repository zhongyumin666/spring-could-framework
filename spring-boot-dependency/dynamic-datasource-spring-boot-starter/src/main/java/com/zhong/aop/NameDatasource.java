package com.zhong.aop;

import javax.sql.DataSource;

/**
 * @author zhongyumin
 * @date 2021/7/21-下午4:31
 */
public interface NameDatasource extends DataSource {
    /**
     * 获取命名唯一键
     * @return
     */
    String getRoutingKey();

    /**
     * 获取角色
     * @return
     */
    RoleType getRole();

    /**
     * 获取名称
     * @return
     */
    String getName();

    /**
     * 获取id
     * @return
     */
    int getId();
}
