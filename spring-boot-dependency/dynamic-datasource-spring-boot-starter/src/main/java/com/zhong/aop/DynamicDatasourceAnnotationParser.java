package com.zhong.aop;

import com.zhong.anno.Master;
import com.zhong.anno.Slave;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.AnnotatedElement;
import java.util.Objects;

/**
 * @author zhongyumin
 * @date 2021/7/15-下午6:05
 */
public class DynamicDatasourceAnnotationParser {


    public boolean isCandidateClass(Class<?> targetClass) {
        return AnnotationUtils.isCandidateClass(targetClass, Master.class)|| AnnotationUtils.isCandidateClass(targetClass, Slave.class);
    }


    public DynamicDatasourceAttribute parseDynamicDatasourceAnnotation(AnnotatedElement element) {
        AnnotationAttributes attributes1 = AnnotatedElementUtils.findMergedAnnotationAttributes(
                element, Master.class, false, false);
        AnnotationAttributes attributes2 = AnnotatedElementUtils.findMergedAnnotationAttributes(
                element, Slave.class, false, false);
        if(Objects.nonNull(attributes1)&&Objects.nonNull(attributes2)){
            throw  new RuntimeException("@Master @Slave only exist one , consider delete one!"+ element.toString());
        }
        if(Objects.nonNull(attributes1)){
           return new DynamicDatasourceAttribute(RoleType.MASTER,attributes1.getNumber("id").intValue(),attributes1.getString("name"),true);
        }
        if(Objects.nonNull(attributes2)){
            return new DynamicDatasourceAttribute(RoleType.SLAVE,attributes1.getNumber("id").intValue(),attributes1.getString("name"),true);
        }
        return null;
    }
}
