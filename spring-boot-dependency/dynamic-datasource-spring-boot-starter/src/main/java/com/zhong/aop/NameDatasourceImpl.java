package com.zhong.aop;

import org.springframework.util.Assert;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

/**
 * @author zhongyumin
 * @date 2021/7/21-下午4:33
 */
public class NameDatasourceImpl implements NameDatasource {
    /**
     * 实际数据源
     */
    private DataSource dataSource;
    /**
     * 数据源类型
     */
    private Class<?> type;

    private RoleType role;
    private String name;
    private int id;

    public NameDatasourceImpl(DataSource dataSource, RoleType role, String name, int id) {
        Assert.notNull(dataSource,"数据源不能为空");
        this.dataSource = dataSource;
        this.role = role==null?RoleType.MASTER:role;
        this.name = name==null?"":name;
        this.id = id;
    }

    @Override
    public String getRoutingKey() {
        return role+name+id;
    }

    @Override
    public RoleType getRole() {
        return role;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return dataSource.getConnection(username,password);
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return dataSource.unwrap(iface);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return dataSource.isWrapperFor(iface);
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return dataSource.getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        dataSource.setLogWriter(out);
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        dataSource.setLoginTimeout(seconds);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return dataSource.getLoginTimeout();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return dataSource.getParentLogger();
    }

    @Override
    public String toString() {
        return "NameDatasourceImpl{" +
                "dataSource=" + dataSource +
                ", type=" + type +
                ", role=" + role +
                ", name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
