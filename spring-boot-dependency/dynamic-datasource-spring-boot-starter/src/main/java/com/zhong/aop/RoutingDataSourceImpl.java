package com.zhong.aop;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import java.util.Objects;

/**
 * 路由数据源
 * @author zhongyumin
 * @date 2021/7/15-下午1:57
 */

public class RoutingDataSourceImpl extends AbstractRoutingDataSource {
    DynamicDatasourceAttributeSourceHolder holder=DynamicDatasourceAttributeSourceHolder.getInstance();
    public Object determineCurrentLookupKey() {
        DynamicDatasourceAttribute currDynamicDatasourceAttribute = holder.getCurrDynamicDatasourceAttribute();
        if(Objects.isNull(currDynamicDatasourceAttribute))
            return null;
        return currDynamicDatasourceAttribute.getLookupKey();
    }
}
