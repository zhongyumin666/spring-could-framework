package com.zhong.aop;

/**
 * 类型,主库还是从库
 * @author zhongyumin
 * @date 2021/7/21-下午4:37
 */
public enum RoleType {
    MASTER,SLAVE;
}
