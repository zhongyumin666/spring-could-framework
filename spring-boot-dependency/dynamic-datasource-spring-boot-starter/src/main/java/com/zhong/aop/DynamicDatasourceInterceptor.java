package com.zhong.aop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * 拦截类
 * @author zhongyumin
 * @date 2021/7/15-下午8:36
 */
public class DynamicDatasourceInterceptor implements MethodInterceptor, Serializable {
    DynamicDatasourceAttributeSource dynamicDatasourceAttributeSource;

    DynamicDatasourceAttributeSourceHolder holder=DynamicDatasourceAttributeSourceHolder.getInstance();


    public DynamicDatasourceInterceptor(DynamicDatasourceAttributeSource dynamicDatasourceAttributeSource) {
        this.dynamicDatasourceAttributeSource = dynamicDatasourceAttributeSource;
    }

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Method method = invocation.getMethod();
        Class<?> clazz = invocation.getThis().getClass();
        if(dynamicDatasourceAttributeSource.isCandidateClass(clazz)){
            DynamicDatasourceAttribute dynamicDatasourceAttribute = dynamicDatasourceAttributeSource.getDynamicDatasourceAttribute(method, clazz);
            if(Objects.nonNull(dynamicDatasourceAttribute)&&dynamicDatasourceAttribute.isAvailable()){
                 holder.setDynamicDatasourceAttribute(dynamicDatasourceAttribute);
            }
        }
        return invocation.proceed();
    }


}
