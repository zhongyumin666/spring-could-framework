package com.zhong.aop;

import org.springframework.aop.ClassFilter;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractBeanFactoryPointcutAdvisor;
import org.springframework.aop.support.StaticMethodMatcherPointcut;

import java.lang.reflect.Method;

/**
 * @author zhongyumin
 * @date 2021/7/15-下午4:30
 */
public abstract class BeanFactoryDynamicDatasourceAttributeSourceAdvisor extends AbstractBeanFactoryPointcutAdvisor  {

    public Pointcut getPointcut() {
        return this.new DynamicDatasourceAttributeSourcePointcut();
    }


    /**
     * 返回匹配资源
     *
     * @return
     */
    protected abstract DynamicDatasourceAttributeSource getDynamicDatasourceAttributeSource();

    /**
     * 切入点
     *
     * @see BeanFactoryDynamicDatasourceAttributeSourceAdvisor
     */
    private class DynamicDatasourceAttributeSourcePointcut extends StaticMethodMatcherPointcut {
        DynamicDatasourceAttributeSourcePointcut() {
            setClassFilter(this.new DynamicDatasourceAttributeSourceClassClassFilter());
        }

        public boolean matches(Method method, Class<?> targetClass) {
            DynamicDatasourceAttributeSource tas = getDynamicDatasourceAttributeSource();
            //tas == null 因为已经通过类过滤器了
            return (tas == null || tas.getDynamicDatasourceAttribute(method, targetClass).isAvailable());
        }


        /**
         * 类过滤器
         *
         * @see DynamicDatasourceAttributeSourcePointcut
         */
        private class DynamicDatasourceAttributeSourceClassClassFilter implements ClassFilter {

            @Override
            public boolean matches(Class<?> clazz) {
                return getDynamicDatasourceAttributeSource() != null && getDynamicDatasourceAttributeSource().isCandidateClass(clazz);
            }
        }
    }
}
