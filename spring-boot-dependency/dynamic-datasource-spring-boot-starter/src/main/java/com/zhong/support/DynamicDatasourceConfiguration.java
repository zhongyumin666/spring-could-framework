package com.zhong.support;

import com.zhong.anno.EnableDynamicDatasource;
import com.zhong.aop.AnnotationDynamicDatasourceAttributeSource;
import com.zhong.aop.BeanFactoryDynamicDatasourceAttributeSourceAdvisor;
import com.zhong.aop.DynamicDatasourceAttributeSource;
import com.zhong.aop.DynamicDatasourceInterceptor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportAware;
import org.springframework.context.annotation.Role;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author zhongyumin
 * @date 2021/7/15-下午8:32
 */
@Configuration
public class DynamicDatasourceConfiguration implements ImportAware {

    protected AnnotationAttributes enableDynamicDatasource;

    /**
     * 基础aop判定资源
     *
     * @return
     */
    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public AnnotationDynamicDatasourceAttributeSource annotationDynamicDatasourceAttributeSource() {
        return new AnnotationDynamicDatasourceAttributeSource();
    }

    /**
     * aop 切面类
     *
     * @return
     */
    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public BeanFactoryDynamicDatasourceAttributeSourceAdvisor beanFactoryDynamicDatasourceAttributeSourceAdvisor() {
        BeanFactoryDynamicDatasourceAttributeSourceAdvisor beanFactoryDynamicDatasourceAttributeSourceAdvisor = new BeanFactoryDynamicDatasourceAttributeSourceAdvisor() {

            @Override
            protected DynamicDatasourceAttributeSource getDynamicDatasourceAttributeSource() {
                return annotationDynamicDatasourceAttributeSource();
            }
        };
        beanFactoryDynamicDatasourceAttributeSourceAdvisor.setAdvice(dynamicDatasourceInterceptor());
        if (enableDynamicDatasource != null) {
            beanFactoryDynamicDatasourceAttributeSourceAdvisor.setOrder(this.enableDynamicDatasource.<Integer>getNumber("order"));
        }
        return beanFactoryDynamicDatasourceAttributeSourceAdvisor;
    }

    /**
     * 拦截器
     *
     * @return
     */
    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public DynamicDatasourceInterceptor dynamicDatasourceInterceptor() {
        return new DynamicDatasourceInterceptor(annotationDynamicDatasourceAttributeSource());
    }


    /**
     * import 回调
     *
     * @param importMetadata
     */
    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        this.enableDynamicDatasource = AnnotationAttributes.fromMap(
                importMetadata.getAnnotationAttributes(EnableDynamicDatasource.class.getName(), false));
        if (this.enableDynamicDatasource == null) {
            throw new IllegalArgumentException(
                    "@EnableDynamicDatasource is not present on importing class " + importMetadata.getClassName());
        }
    }
}
