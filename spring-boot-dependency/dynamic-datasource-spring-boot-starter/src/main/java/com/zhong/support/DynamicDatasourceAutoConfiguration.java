package com.zhong.support;

import com.zhong.aop.NameDatasource;
import com.zhong.aop.NameDatasourceImpl;
import com.zhong.aop.RoutingDataSourceImpl;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.util.CollectionUtils;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhongyumin
 * @date 2021/7/20-上午10:16
 */
@Configuration
@EnableConfigurationProperties(DynamicDatasourceProperties.class)
@ConditionalOnClass({DataSource.class, EmbeddedDatabaseType.class})
public class DynamicDatasourceAutoConfiguration {
    @Autowired
    DynamicDatasourceProperties dynamicDatasourceProperties;

    @Bean
    @Primary
    public DataSource dynamicDatasource(ObjectProvider<DynamicDatasourceCustomizer> customizers) {
        List<DynamicDatasourceProperties.DatasourceProperties> datasource = dynamicDatasourceProperties.getDatasource();
        Map<String, NameDatasource> map = new HashMap<>();
        if (CollectionUtils.isEmpty(datasource)) {
            throw new RuntimeException("at least one data source is required!");
        }
        for (DynamicDatasourceProperties.DatasourceProperties properties : datasource) {
            //数据源
            DataSource dataSource = DataSourceBuilder
                    .create(properties.getClassLoader())
                    .type(properties.getType())
                    .username(properties.getUsername())
                    .driverClassName(properties.getDriverClassName())
                    .password(properties.getPassword())
                    .url(properties.getUrl())
                    .build();
            //唯一键
            String routingKey = properties.getRoutingKey();
            //转换为命名空间数据源
            NameDatasource nameDatasource = new NameDatasourceImpl(dataSource, properties.getRole(), properties.getName(), properties.getId());
            map.put(routingKey, nameDatasource);
        }
        customizers.orderedStream().forEach(customizer -> customizer.customize(map));

        RoutingDataSourceImpl routingDataSource = new RoutingDataSourceImpl();
        routingDataSource.setTargetDataSources((Map) map);
        //添加默认数据源
        List<DynamicDatasourceProperties.DatasourceProperties> list = datasource.stream().filter(DynamicDatasourceProperties.DatasourceProperties::isDefault).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(list) && list.size() > 1) {
            throw new RuntimeException("at most, only one data source is required");
        }
        //有设置默认的取默认的,没有取配置文件配置的第一个数据源
        NameDatasource defaultNameDatasource;
        if (!CollectionUtils.isEmpty(list)) {
            DynamicDatasourceProperties.DatasourceProperties properties = list.get(0);
            DataSource dataSource = map.get(properties.getRoutingKey());
            defaultNameDatasource = new NameDatasourceImpl(dataSource, properties.getRole(), properties.getName(), properties.getId());
        } else {
            DynamicDatasourceProperties.DatasourceProperties properties = datasource.get(0);
            DataSource dataSource = map.get(datasource.get(0).getRoutingKey());
            defaultNameDatasource = new NameDatasourceImpl(dataSource, properties.getRole(), properties.getName(), properties.getId());
        }
        routingDataSource.setDefaultTargetDataSource(defaultNameDatasource);
        return routingDataSource;
    }

}
