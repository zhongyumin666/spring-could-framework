package com.zhong.support;

import com.zhong.aop.DynamicDatasourceAttribute;
import com.zhong.aop.RoleType;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author zhongyumin
 * @date 2021/7/20-上午10:26
 */
@ConfigurationProperties(prefix = "spring.dynamic")
public class DynamicDatasourceProperties {


    @NestedConfigurationProperty
    List<DatasourceProperties> datasource;

    public List<DatasourceProperties> getDatasource() {
        return datasource;
    }

    public void setDatasource(List<DatasourceProperties> datasource) {
        this.datasource = datasource;
    }

    /**
     * role + name + id 唯一定位一个数据源
     */
    public static class DatasourceProperties {
        /**
         * 数据源角色
         * @note 默认主库
         *
         * @see RoleType
         */
        private RoleType role = RoleType.MASTER;

        /**
         * 数据源id
         * @note 默认0
         *
         * @see DynamicDatasourceAttribute
         */
        private int id = 0;

        /**
         * 数据源名称
         * @note 默认""
         * @see DynamicDatasourceAttribute
         */
        private String name = "";

        /**
         * 是否是默认数据源
         * @note 如果一个都没有设置为true
         * 当routingkey找不到对应数据源的时候
         * 默认为配置的第一个库为默认库
         */
        private boolean isDefault = false;


        private ClassLoader classLoader;


        private Class<? extends DataSource> type;


        private String driverClassName;


        private String url;


        private String username;


        private String password;

        public String getRoutingKey() {
            return role + name + id;
        }


        public RoleType getRole() {
            return role;
        }

        public void setRole(RoleType role) {
            this.role = role;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ClassLoader getClassLoader() {
            return classLoader;
        }

        public void setClassLoader(ClassLoader classLoader) {
            this.classLoader = classLoader;
        }

        public Class<? extends DataSource> getType() {
            return type;
        }

        public void setType(Class<? extends DataSource> type) {
            this.type = type;
        }

        public String getDriverClassName() {
            return driverClassName;
        }

        public void setDriverClassName(String driverClassName) {
            this.driverClassName = driverClassName;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public boolean isDefault() {
            return isDefault;
        }

        public void setDefault(boolean aDefault) {
            isDefault = aDefault;
        }

    }
}
