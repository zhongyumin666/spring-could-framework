package com.zhong.support;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author zhongyumin
 * @date 2021/7/15-下午3:08
 */

public class DynamicDatasourceSelector implements ImportSelector {

    public String[] selectImports(AnnotationMetadata importingClassMetadata) {

        return new String[]{"com.zhong.support.DynamicDatasourceConfiguration","com.zhong.support.DynamicDatasourceAutoConfiguration"};
    }
}
