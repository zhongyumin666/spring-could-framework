package com.zhong.support;

import com.zhong.aop.NameDatasource;

import java.util.Map;

/**
 * 多数据源自动配置的定制化类
 * 在此可以更改数据源的配置
 * 1:如添加自定义数据源
 * 2:如更改一些数据源的配置
 *
 * @author zhongyumin
 * @date 2021/7/20-上午11:48
 */
@FunctionalInterface
public interface DynamicDatasourceCustomizer {
    /**
     * Customize datasource
     *
     * @param dataSourceMap
     */
    void customize(Map<String, NameDatasource> dataSourceMap);
}
