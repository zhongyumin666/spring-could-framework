package com.zhong.anno;

import com.zhong.support.DynamicDatasourceSelector;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;

import java.lang.annotation.*;

/**
 * @author zhongyumin
 * @date 2021/7/15-下午2:01
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(DynamicDatasourceSelector.class)
public @interface EnableDynamicDatasource {

    /**
     * @note 必须和导入AutoProxyRegistrar组建的一起工作
     * 表明代理是基于接口代理还是基于类去代理
     * 如果是true则会使用CGLIB代理基于类去代理
     * 如果是false则会使用JDK基于接口的动态代理去代理
     * @note dataSourceTransaction 支持aspectj的字节码前置加工去作为代理
     * 但本组件目前暂不支持对aspectj字节码加工去代理
     * @return
     */
    boolean proxyTargetClass() default false;

    /**
     * 默认优先级比datasourceTransactionManager多一个,表示发生在它之前
     * @return
     */
   int order() default Ordered.LOWEST_PRECEDENCE-1;
}
