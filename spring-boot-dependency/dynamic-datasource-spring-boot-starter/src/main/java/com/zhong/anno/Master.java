package com.zhong.anno;

import java.lang.annotation.*;

/**
 * @note 有名称默认用名称找,无名称默认用id找
 * 标明主数据源
 * id 默认为第一个0
 * @author zhongyumin
 * @date 2021/7/15-下午1:41
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Master {

    int id() default 0;
    //主库名称
    String name() default "";
}
