package com.zhong.test.service.entity;

import lombok.Data;

/**
 * @author zhongyumin
 * @date 2021/7/20-下午1:50
 */
@Data
public class Student {
    private Integer id;
    private String name;
    private Integer age;
    private String gender;
}
