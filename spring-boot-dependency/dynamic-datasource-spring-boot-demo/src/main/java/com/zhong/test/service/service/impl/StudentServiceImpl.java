package com.zhong.test.service.service.impl;

import com.zhong.anno.Master;
import com.zhong.test.service.entity.Student;
import com.zhong.test.service.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author zhongyumin
 * @date 2021/7/20-下午1:58
 */
@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    JdbcTemplate jdbcTemplate;
   @Autowired
    StudentService studentService;
    @Transactional(rollbackFor = Exception.class)
    @Master(name = "study")
    public void add(Student student) {
        String sql="INSERT student(id,name,age) values(%s,'%s',%s)";
        String format = String.format(sql, student.getId(), student.getName(), student.getAge(), student.getGender());
        jdbcTemplate.execute(format);
    }

    @Override
    @Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRES_NEW)
    @Master(name = "study")
    public void addTwo(Student student) {
        add(student);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Master(name = "study1")
    public void addOne(Student student) {
        add(student);
        studentService.addTwo(student);
        throw new RuntimeException("addOne 失败");
    }


}
