package com.zhong.test.service.service;


import com.zhong.test.service.entity.Student;

/**
 * @author zhongyumin
 * @date 2021/7/20-下午1:49
 */
public interface StudentService {
    void  add(Student student);
    void  addTwo(Student student);
    void  addOne(Student student);
}
