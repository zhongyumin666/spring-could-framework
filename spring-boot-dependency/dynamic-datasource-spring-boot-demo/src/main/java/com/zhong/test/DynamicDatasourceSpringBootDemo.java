package com.zhong.test;

import com.zhong.anno.EnableDynamicDatasource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author zhongyumin
 * @date 2021/7/20-下午1:44
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableDynamicDatasource
public class DynamicDatasourceSpringBootDemo {
    public static void main(String[] args) {
        SpringApplication.run(DynamicDatasourceSpringBootDemo.class,args);
    }
}
