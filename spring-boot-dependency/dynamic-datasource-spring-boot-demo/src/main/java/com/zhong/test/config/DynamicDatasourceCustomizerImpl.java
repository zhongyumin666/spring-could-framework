package com.zhong.test.config;

import com.zhong.aop.NameDatasource;
import com.zhong.support.DynamicDatasourceCustomizer;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhongyumin
 * @date 2021/7/21-下午4:18
 */
@Component
public class DynamicDatasourceCustomizerImpl implements DynamicDatasourceCustomizer {
    @Override
    public void customize(Map<String, NameDatasource> dataSourceMap) {
        dataSourceMap.forEach((k,v)->{
            System.err.println(k+"="+v);
        });
    }
}
