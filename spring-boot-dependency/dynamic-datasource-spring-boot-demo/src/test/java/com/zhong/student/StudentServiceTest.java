package com.zhong.student;

import com.zhong.test.DynamicDatasourceSpringBootDemo;
import com.zhong.test.service.entity.Student;
import com.zhong.test.service.service.StudentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

/**
 * @author zhongyumin
 * @date 2021/7/20-下午2:04
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE,classes = DynamicDatasourceSpringBootDemo.class)
public class StudentServiceTest {
    @Autowired
    StudentService studentService;
    @Autowired
    ApplicationContext applicationContext;

    /**
     * 测试数据库选择
     * @Transactional(rollbackFor = Exception.class)
     *     @Master(name = "study")
     *     public void add(Student student) {
     *         String sql="INSERT student(id,name,age) values(%s,'%s',%s)";
     *         String format = String.format(sql, student.getId(), student.getName(), student.getAge(), student.getGender());
     *         jdbcTemplate.execute(format);
     *     }
     */
    @Test
    public void studentAdd(){
        Student student=new Student();
        student.setId(2);
        student.setName("zhong");
        student.setAge(10);
        student.setGender("男");
        studentService.add(student);
    }

    /**
     * 测试多数据库插入
     * @note 多个嵌套事务第一层异常了,需要注意里面的是不能回滚的
     *  @Override
     *     @Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRES_NEW)
     *     @Master(name = "study")
     *     public void addTwo(Student student) {
     *         add(student);
     *     }
     *
     *     @Override
     *     @Transactional(rollbackFor = Exception.class)
     *     @Master(name = "study1")
     *     public void addOne(Student student) {
     *         add(student);
     *         studentService.addTwo(student);
     *         //@基础知识:addTwo 不能回滚因为执行没有异常已经被提交了,不仅仅是多机数据源
     *         //但是addTwo 异常了会导致外层回滚
     *         throw new RuntimeException("addOne 失败");
     *     }
     */
    @Test
    public void studentAddTwo(){
        Student student=new Student();
        student.setId(1);
        student.setName("zhong");
        student.setAge(10);
        student.setGender("男");
        studentService.addOne(student);
    }
}
